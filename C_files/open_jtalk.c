/* ----------------------------------------------------------------- */
/*           The Japanese TTS System "Open JTalk"                    */
/*           developed by HTS Working Group                          */
/*           http://open-jtalk.sourceforge.net/                      */
/* ----------------------------------------------------------------- */
/*                                                                   */
/*  Copyright (c) 2008-2013  Nagoya Institute of Technology          */
/*                           Department of Computer Science          */
/*                                                                   */
/* All rights reserved.                                              */
/*                                                                   */
/* Redistribution and use in source and binary forms, with or        */
/* without modification, are permitted provided that the following   */
/* conditions are met:                                               */
/*                                                                   */
/* - Redistributions of source code must retain the above copyright  */
/*   notice, this list of conditions and the following disclaimer.   */
/* - Redistributions in binary form must reproduce the above         */
/*   copyright notice, this list of conditions and the following     */
/*   disclaimer in the documentation and/or other materials provided */
/*   with the distribution.                                          */
/* - Neither the name of the HTS working group nor the names of its  */
/*   contributors may be used to endorse or promote products derived */
/*   from this software without specific prior written permission.   */
/*                                                                   */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND            */
/* CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,       */
/* INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF          */
/* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE          */
/* DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS */
/* BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,          */
/* EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED   */
/* TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,     */
/* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON */
/* ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,   */
/* OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY    */
/* OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE           */
/* POSSIBILITY OF SUCH DAMAGE.                                       */
/* ----------------------------------------------------------------- */

#ifndef OPEN_JTALK_C
#define OPEN_JTALK_C

#ifdef __cplusplus
#define OPEN_JTALK_C_START extern "C" {
#define OPEN_JTALK_C_END   }
#else
#define OPEN_JTALK_C_START
#define OPEN_JTALK_C_END
#endif                          /* __CPLUSPLUS */

OPEN_JTALK_C_START;

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <math.h>

/* Main headers */
#include "mecab.h"
#include "njd.h"
#include "jpcommon.h"
#include "HTS_engine.h"

/* Sub headers */
#include "text2mecab.h"
#include "mecab2njd.h"
#include "njd_set_pronunciation.h"
#include "njd_set_digit.h"
#include "njd_set_accent_phrase.h"
#include "njd_set_accent_type.h"
#include "njd_set_unvoiced_vowel.h"
#include "njd_set_long_vowel.h"
#include "njd2jpcommon.h"

//modified this size of MAXBUFLEN
#define MAXBUFLEN 4096

//the idea of using this structure is contributed by Bi Yu
typedef struct
{
   //dictionary directory
   char dic[256];
   //HTS Voice filename
   char hts[256];
   //output trace filename
   char ot[256];
   //output WAV filename
   char ow[256];
   //input text filename
   char txtfn[256];
   //input string
   char textString[4096];
   //sampling frequency
   int s;
   //frame period
   int p;
   //all pass constant
   float a;
   //postfiltering coefficient
   float b;
   //speech speed rate
   float r;
   //additional half-tone
   float fm;
   //voiced/unvoiced threshold
   float u;
   //weight of GV for spectrum
   float jm;
   //weight of GV for LF0
   float jf;
   //audio buffer size
   int z;
}jtPramater;
//behaviour
//(as defined by wing0wind):
//-every parameters must have defined value
// -this includes all options, output WAV file name, output trace file name
//-if the length of textString is more than 0, synthesize from it
//-if the length of textString is 0, synthesize from the content of txtfn
//(suggested correction):
//-people might want to simply use the default value
//-default value decided by Open JTalk:
// Parameters  Default  Min  Max
// b           0.0      0.0  1.0
// r           1.0      0.0  +INF
// fm          0.0      -INF +INF
// u           0.5      0.0  1.0
// jm          1.0      0.0  +INF
// jf          1.0      0.0  +INF
// z           0        0    +INF
//-problem: the default values of s, p, a depend on the HTS Voice
//-create a function InitParameter(*jtp, *voice)

typedef struct _Open_JTalk {
   Mecab mecab;
   NJD njd;
   JPCommon jpcommon;
   HTS_Engine engine;
} Open_JTalk;

static void Open_JTalk_initialize(Open_JTalk * open_jtalk)
{
   Mecab_initialize(&open_jtalk->mecab);
   NJD_initialize(&open_jtalk->njd);
   JPCommon_initialize(&open_jtalk->jpcommon);
   HTS_Engine_initialize(&open_jtalk->engine);
}

static void Open_JTalk_clear(Open_JTalk * open_jtalk)
{
   Mecab_clear(&open_jtalk->mecab);
   NJD_clear(&open_jtalk->njd);
   JPCommon_clear(&open_jtalk->jpcommon);
   HTS_Engine_clear(&open_jtalk->engine);
}

static int Open_JTalk_load(Open_JTalk * open_jtalk, char *dn_mecab, char *fn_voice)
{
   if (Mecab_load(&open_jtalk->mecab, dn_mecab) != TRUE) {
      Open_JTalk_clear(open_jtalk);
      return 0;
   }
   if (HTS_Engine_load(&open_jtalk->engine, &fn_voice, 1) != TRUE) {
      Open_JTalk_clear(open_jtalk);
      return 0;
   }
   return 1;
}

static void Open_JTalk_set_sampling_frequency(Open_JTalk * open_jtalk, size_t i)
{
   HTS_Engine_set_sampling_frequency(&open_jtalk->engine, i);
}

static void Open_JTalk_set_fperiod(Open_JTalk * open_jtalk, size_t i)
{
   HTS_Engine_set_fperiod(&open_jtalk->engine, i);
}

static void Open_JTalk_set_alpha(Open_JTalk * open_jtalk, double f)
{
   HTS_Engine_set_alpha(&open_jtalk->engine, f);
}

static void Open_JTalk_set_beta(Open_JTalk * open_jtalk, double f)
{
   HTS_Engine_set_beta(&open_jtalk->engine, f);
}

static void Open_JTalk_set_speed(Open_JTalk * open_jtalk, double f)
{
   HTS_Engine_set_speed(&open_jtalk->engine, f);
}

static void Open_JTalk_add_half_tone(Open_JTalk * open_jtalk, double f)
{
   HTS_Engine_add_half_tone(&open_jtalk->engine, f);
}

static void Open_JTalk_set_msd_threshold(Open_JTalk * open_jtalk, size_t i, double f)
{
   HTS_Engine_set_msd_threshold(&open_jtalk->engine, i, f);
}

static void Open_JTalk_set_gv_weight(Open_JTalk * open_jtalk, size_t i, double f)
{
   HTS_Engine_set_gv_weight(&open_jtalk->engine, i, f);
}

static void Open_JTalk_set_audio_buff_size(Open_JTalk * open_jtalk, size_t i)
{
   HTS_Engine_set_audio_buff_size(&open_jtalk->engine, i);
}

static int Open_JTalk_synthesis(Open_JTalk * open_jtalk, const char *txt, FILE * wavfp,
                                FILE * logfp)
{
   int result = 0;
   char buff[MAXBUFLEN];

   text2mecab(buff, txt);
   Mecab_analysis(&open_jtalk->mecab, buff);
   mecab2njd(&open_jtalk->njd, Mecab_get_feature(&open_jtalk->mecab),
             Mecab_get_size(&open_jtalk->mecab));
   njd_set_pronunciation(&open_jtalk->njd);
   njd_set_digit(&open_jtalk->njd);
   njd_set_accent_phrase(&open_jtalk->njd);
   njd_set_accent_type(&open_jtalk->njd);
   njd_set_unvoiced_vowel(&open_jtalk->njd);
   njd_set_long_vowel(&open_jtalk->njd);
   njd2jpcommon(&open_jtalk->jpcommon, &open_jtalk->njd);
   JPCommon_make_label(&open_jtalk->jpcommon);
   if (JPCommon_get_label_size(&open_jtalk->jpcommon) > 2) {
      if (HTS_Engine_synthesize_from_strings
          (&open_jtalk->engine, JPCommon_get_label_feature(&open_jtalk->jpcommon),
           JPCommon_get_label_size(&open_jtalk->jpcommon)) == TRUE)
         result = 1;
      if (wavfp != NULL)
         HTS_Engine_save_riff(&open_jtalk->engine, wavfp);
      if (logfp != NULL) {
         fprintf(logfp, "[Text analysis result]\n");
         NJD_fprint(&open_jtalk->njd, logfp);
         fprintf(logfp, "\n[Output label]\n");
         HTS_Engine_save_label(&open_jtalk->engine, logfp);
         fprintf(logfp, "\n");
         HTS_Engine_save_information(&open_jtalk->engine, logfp);
      }
      HTS_Engine_refresh(&open_jtalk->engine);
   }
   JPCommon_refresh(&open_jtalk->jpcommon);
   NJD_refresh(&open_jtalk->njd);
   Mecab_refresh(&open_jtalk->mecab);

   return result;
}

static void usage()
{
   fprintf(stderr, "The Japanese TTS System \"Open JTalk\"\n");
   fprintf(stderr, "Version 1.07 (http://open-jtalk.sourceforge.net/)\n");
   fprintf(stderr, "Copyright (C) 2008-2013 Nagoya Institute of Technology\n");
   fprintf(stderr, "All rights reserved.\n");
   fprintf(stderr, "\n");
   fprintf(stderr, "%s", HTS_COPYRIGHT);
   fprintf(stderr, "\n");
   fprintf(stderr, "Yet Another Part-of-Speech and Morphological Analyzer \"Mecab\"\n");
   fprintf(stderr, "Version 0.994 (http://mecab.sourceforge.net/)\n");
   fprintf(stderr, "Copyright (C) 2001-2008 Taku Kudo\n");
   fprintf(stderr, "              2004-2008 Nippon Telegraph and Telephone Corporation\n");
   fprintf(stderr, "All rights reserved.\n");
   fprintf(stderr, "\n");
   fprintf(stderr, "NAIST Japanese Dictionary\n");
   fprintf(stderr, "Version 0.6.1-20090630 (http://naist-jdic.sourceforge.jp/)\n");
   fprintf(stderr, "Copyright (C) 2009 Nara Institute of Science and Technology\n");
   fprintf(stderr, "All rights reserved.\n");
   fprintf(stderr, "\n");
   fprintf(stderr, "open_jtalk - The Japanese TTS system \"Open JTalk\"\n");
   fprintf(stderr, "\n");
   fprintf(stderr, "  usage:\n");
   fprintf(stderr, "       open_jtalk [ options ] [ infile ] \n");
   fprintf(stderr,
           "  options:                                                                   [  def][ min-- max]\n");
   fprintf(stderr,
           "    -x  dir        : dictionary directory                                    [  N/A]\n");
   fprintf(stderr,
           "    -m  htsvoice   : HTS voice files                                         [  N/A]\n");
   fprintf(stderr,
           "    -ow s          : filename of output wav audio (generated speech)         [  N/A]\n");
   fprintf(stderr,
           "    -ot s          : filename of output trace information                    [  N/A]\n");
   fprintf(stderr,
           "    -s  i          : sampling frequency                                      [ auto][   1--    ]\n");
   fprintf(stderr,
           "    -p  i          : frame period (point)                                    [ auto][   1--    ]\n");
   fprintf(stderr,
           "    -a  f          : all-pass constant                                       [ auto][ 0.0-- 1.0]\n");
   fprintf(stderr,
           "    -b  f          : postfiltering coefficient                               [  0.0][ 0.0-- 1.0]\n");
   fprintf(stderr,
           "    -r  f          : speech speed rate                                       [  1.0][ 0.0--    ]\n");
   fprintf(stderr,
           "    -fm f          : additional half-tone                                    [  0.0][    --    ]\n");
   fprintf(stderr,
           "    -u  f          : voiced/unvoiced threshold                               [  0.5][ 0.0-- 1.0]\n");
   fprintf(stderr,
           "    -jm f          : weight of GV for spectrum                               [  1.0][ 0.0--    ]\n");
   fprintf(stderr,
           "    -jf f          : weight of GV for log F0                                 [  1.0][ 0.0--    ]\n");
#ifdef HTS_MELP
   fprintf(stderr,
           "    -jl f          : weight of GV for low-pass filter                        [  1.0][ 0.0--    ]\n");
#endif                          /* HTS_MELP */
   fprintf(stderr,
           "    -z  i          : audio buffer size (if i==0, turn off)                   [    0][   0--    ]\n");
   fprintf(stderr,
           "  additional options (modes):\n");
   fprintf(stderr,
           "    +n             : (normal mode) synthesize from Japanese text\n");
   fprintf(stderr,
           "    +w  xml        : (write mode)  write text info to XML file then quit     [  N/A]\n");
   fprintf(stderr,
           "    +r             : (read mode)   synthesize from XML\n");
   fprintf(stderr, "  infile:\n");
   fprintf(stderr,
           "    text file                                                                [stdin]\n");
   fprintf(stderr, "\n");

   exit(0);
}

//--------------------------------------------------------------------
//Modification made by Hafiyan Prafianto from this line until line 997
//--------------------------------------------------------------------
//Modification begins
//-------------------

//-----------------------
//code to create XML tree
//-----------------------

static void Phoneme2XML(JPCommonLabelPhoneme *phoneme, FILE *output)
{
   fprintf(output, "<phoneme phonemename='%s'/>\n", phoneme->phoneme);
}

static void Mora2XML(JPCommonLabelMora *mora, FILE *output)
{
   JPCommonLabelPhoneme *current = mora->head;
   
   fprintf(output, "<mora moraname='%s'>\n", mora->mora);
   do{
      Phoneme2XML(current, output);
      if(current != mora->tail) current = current->next;
      else break;
   }while(1);
   fprintf(output, "</mora>\n");
}

static void Word2XML(JPCommonLabelWord *word, FILE *output)
{
   JPCommonLabelMora *current = word->head;
   
   fprintf(output, "<word pron='%s' pos='%s' ctype='%s' cform='%s'>\n",
      word->pron, word->pos, word->ctype, word->cform);
   do{
      Mora2XML(current, output);
      if(current != word->tail) current = current->next;
      else break;
   }while(1);
   fprintf(output, "</word>\n");
}

static void AccentPhrase2XML(JPCommonLabelAccentPhrase *accent, FILE *output)
{
   JPCommonLabelWord *current = accent->head;
   
   fprintf(output, "<accentphrase accent='%d' emotion='%s'>\n",
      accent->accent, accent->emotion);
   do{
      Word2XML(current, output);
      if(current != accent->tail) current = current->next;
      else break;
   }while(1);
   fprintf(output, "</accentphrase>\n");
}

static void BreathGroup2XML(JPCommonLabelBreathGroup *breath, FILE *output)
{
   JPCommonLabelAccentPhrase *current = breath->head;
   
   fprintf(output, "<breathgroup>\n");
   do{
      AccentPhrase2XML(current, output);
      if(current != breath->tail) current = current->next;
      else break;
   }while(1);
   fprintf(output, "</breathgroup>\n");
}

static void JPCommon2XML(JPCommon *jpcommon, FILE *output, const char *encoding)
{
   JPCommonLabelBreathGroup *current = jpcommon->label->breath_head;
   
   fprintf(output, "<?xml version='1.0' encoding='%s'?>\n", encoding);
   fprintf(output, "<utterance>\n");
   do{
      BreathGroup2XML(current, output);
      if(current != jpcommon->label->breath_tail) current = current->next;
      else break;
   }while(1);
   fprintf(output, "</utterance>\n");
}

//Creating JPCommonTree of a string using MeCab dictionary
//parameters:
//dn_dict: the path of the dictionary containing MeCab dictionary
//txt: the string to be analyzed
//encoding: well, the encoding. It cannot be changed easily though
//output: the XML output file pointer
static void CreateJPCommonTree(const char *dn_dict, const char *txt, const char *encoding, FILE *output)
{
   Open_JTalk temp;
   Open_JTalk *open_jtalk;
   
   char buff[MAXBUFLEN];

   //from main
   open_jtalk = &temp;
   Open_JTalk_initialize(open_jtalk);

   //from load
   if (Mecab_load(&open_jtalk->mecab, dn_dict) != TRUE) {
      Open_JTalk_clear(open_jtalk);
      exit(0);
   }
   
   //from synthesize
   text2mecab(buff, txt);
   Mecab_analysis(&open_jtalk->mecab, buff);
   
   mecab2njd(&open_jtalk->njd, Mecab_get_feature(&open_jtalk->mecab),
             Mecab_get_size(&open_jtalk->mecab));
   njd_set_pronunciation(&open_jtalk->njd);
   njd_set_digit(&open_jtalk->njd);
   njd_set_accent_phrase(&open_jtalk->njd);
   njd_set_accent_type(&open_jtalk->njd);
   njd_set_unvoiced_vowel(&open_jtalk->njd);
   njd_set_long_vowel(&open_jtalk->njd);
   //fprintf(fp, "%s\n", open_jtalk->njd.head->string); //sekai (kanji)
   njd2jpcommon(&open_jtalk->jpcommon, &open_jtalk->njd);
   //fprintf(fp, "%s\n", open_jtalk->jpcommon.head->pron); //sekai (kana)
   
   //a new function in jpcommon.c
   JPCommon_make_tree(&open_jtalk->jpcommon);
   
   JPCommon2XML(&open_jtalk->jpcommon, output, encoding);
   
   JPCommon_refresh(&open_jtalk->jpcommon);
   NJD_refresh(&open_jtalk->njd);
   Mecab_refresh(&open_jtalk->mecab);
}

//---------------------
//code to read from XML
//---------------------

static void XML2JPCommon(FILE *input, JPCommon *jpcommon)
{
   JPCommonLabelPhoneme *new_phoneme;
   JPCommonLabelMora *new_mora;
   JPCommonLabelWord *new_word;
   JPCommonLabelAccentPhrase *new_accent;
   JPCommonLabelBreathGroup *new_breath;
   
   char line[MAXBUFLEN];
   int a, b, c, d, e, f, g;
   char sa[32], sb[32], sc[32], sd[32];
   char **strings;
   
   int i, total_phoneme;
   
   fgets(line, MAXBUFLEN, input); //ignore the first line
   fscanf(input,
      "<utterance total_breath='%d' total_accent='%d' total_word='%d' total_mora='%d' total_phoneme='%d' >\n",
      &e, &d, &c, &b, &a);
   new_phoneme = (JPCommonLabelPhoneme*)calloc(a, sizeof(JPCommonLabelPhoneme));
      //I need all pointers to be NULL by default
   new_mora = (JPCommonLabelMora*)malloc(b * sizeof(JPCommonLabelMora));
   new_word = (JPCommonLabelWord*)malloc(c * sizeof(JPCommonLabelWord));
   new_accent = (JPCommonLabelAccentPhrase*)malloc(d * sizeof(JPCommonLabelAccentPhrase));
   new_breath = (JPCommonLabelBreathGroup*)malloc(e * sizeof(JPCommonLabelBreathGroup));
   
   total_phoneme = a;
   
   //number of strings:
   //accent: 1
   //word: 4
   //mora: 1
   //phoneme: 1
   //pau: last 1
   strings = malloc((d + 4 * c + b + a + 1) * sizeof(char*));
   for (i = 0; i < (d + 4 * c + b + a + 1); i++)
      strings[i] = malloc((32) * sizeof(char));
   
   //main part
   
   jpcommon->label->size = 0;
   jpcommon->label->feature = NULL;
   jpcommon->label->breath_head = &new_breath[0];
   jpcommon->label->accent_head = &new_accent[0];
   jpcommon->label->word_head = &new_word[0];
   jpcommon->label->mora_head = &new_mora[0];
   jpcommon->label->phoneme_head = &new_phoneme[0];
   jpcommon->label->breath_tail = &new_breath[a - 1];
   jpcommon->label->accent_tail = &new_accent[b - 1];
   jpcommon->label->word_tail = &new_word[c - 1];
   jpcommon->label->mora_tail = &new_mora[d - 1];
   jpcommon->label->phoneme_tail = &new_phoneme[e - 1];
   jpcommon->label->short_pause_flag = 0;
   
   strcpy(strings[0], "pau");
   i = 1;
   while(fgets(line, MAXBUFLEN, input) != NULL){
      //fprintf(stdout, "line: %s", line);
      switch(line[1]){
         case '/':
            continue;
         case 'b': //breath group
            sscanf(line,
               "<breathgroup current_id='%d' prev_id='%d' next_id='%d' up_id='%d' head_id='%d' tail_id='%d' >\n",
               &a, &b, &c, &d, &e, &f);
            new_breath[a].prev = b >= 0 ? new_breath + b : NULL;
            new_breath[a].next = c >= 0 ? new_breath + c : NULL;
            new_breath[a].head = new_accent + e;
            new_breath[a].tail = new_accent + f;
            break;
         case 'a': //accent phrase
            sscanf(line,
               "<accentphrase accent='%d' emotion='%[^']' current_id='%d' prev_id='%d' next_id='%d' up_id='%d' head_id='%d' tail_id='%d' >\n",
               &a, sa, &b, &c, &d, &e, &f, &g);
            if( strcmp(sa, "(null)") != 0){
               strcpy(strings[i], sa);
            }else{
               strings[i] = NULL;
            }
            i++;
            new_accent[b].accent = a;
            new_accent[b].emotion = strings[i - 1];
            new_accent[b].prev = c >= 0 ? new_accent + c : NULL;
            new_accent[b].next = d >= 0 ? new_accent + d : NULL;
            new_accent[b].up = new_breath + e;
            new_accent[b].head = new_word + f;
            new_accent[b].tail = new_word + g;
            break;
         case 'w': //word
            sscanf(line,
               "<word pron='%[^']' pos='%[^']' ctype='%[^']' cform='%[^']' current_id='%d' prev_id='%d' next_id='%d' up_id='%d' head_id='%d' tail_id='%d' >",
               sa, sb, sc, sd, &a, &b, &c, &d, &e, &f);
            strcpy(strings[i], sa); i++;
            strcpy(strings[i], sb); i++;
            strcpy(strings[i], sc); i++;
            strcpy(strings[i], sd); i++;
            new_word[a].pron = strings[i - 4];
            new_word[a].pos = strings[i - 3];
            new_word[a].ctype = strings[i - 2];
            new_word[a].cform = strings[i - 1];
            new_word[a].prev = b >= 0 ? new_word + b : NULL;
            new_word[a].next = c >= 0 ? new_word + c : NULL;
            new_word[a].up = new_accent + d;
            new_word[a].head = new_mora + e;
            new_word[a].tail = new_mora + f;
            break;
         case 'm': //mora
            sscanf(line,
               "<mora moraname='%[^']' current_id='%d' prev_id='%d' next_id='%d' up_id='%d' head_id='%d' tail_id='%d' >\n",
               sa, &a, &b, &c, &d, &e, &f);
            strcpy(strings[i], sa); i++;
            new_mora[a].mora = strings[i - 1];
            new_mora[a].prev = b >= 0 ? new_mora + b : NULL;
            new_mora[a].next = c >= 0 ? new_mora + c : NULL;
            new_mora[a].up = new_word + d;
            new_mora[a].head = new_phoneme + e;
            new_mora[a].tail = new_phoneme + f;
            break;
         case 'p': //phoneme
            sscanf(line,
               "<phoneme phonemename='%[^']' current_id='%d' prev_id='%d' next_id='%d' up_id='%d' />\n",
               sa, &a, &b, &c, &d);
            strcpy(strings[i], sa); i++;
            new_phoneme[a].phoneme = strings[i - 1];
            new_phoneme[a].prev = b >= 0 ? new_phoneme + b : NULL;
            new_phoneme[a].next = c >= 0 ? new_phoneme + c : NULL;
            new_phoneme[a].up = new_mora + d;
            break;
      }
   }
   
   //setting the prevs and the nexts of the pause phonemes
   for(i = 0; i < total_phoneme; i++){
      if( new_phoneme[i].phoneme == NULL){
         new_phoneme[i].phoneme = strings[0];
         new_phoneme[i].prev    = &new_phoneme[i - 1];
         new_phoneme[i].next    = &new_phoneme[i + 1];
         new_phoneme[i].up      = NULL;
      }
   }
}

static int SynthesizeFromTree(Open_JTalk * open_jtalk, FILE *input, FILE * wavfp,
                                FILE * logfp)
{
   int result = 0;
   
   //initialize
   //from jpcommon.c:JPCommon_make_label
   JPCommon_init_label(&open_jtalk->jpcommon);
   
   //read tree
   XML2JPCommon(input, &open_jtalk->jpcommon);
   
   //edited from the normal synthesis
   JPCommon_make_label_from_tree(&open_jtalk->jpcommon);
   
   if (JPCommon_get_label_size(&open_jtalk->jpcommon) > 2) {
      if (HTS_Engine_synthesize_from_strings
          (&open_jtalk->engine, JPCommon_get_label_feature(&open_jtalk->jpcommon),
           JPCommon_get_label_size(&open_jtalk->jpcommon)) == TRUE)
         result = 1;
      if (wavfp != NULL)
         HTS_Engine_save_riff(&open_jtalk->engine, wavfp);
      if (logfp != NULL) {
         fprintf(logfp, "[Text analysis result]\n");
         NJD_fprint(&open_jtalk->njd, logfp);
         fprintf(logfp, "\n[Output label]\n");
         HTS_Engine_save_label(&open_jtalk->engine, logfp);
         fprintf(logfp, "\n");
         HTS_Engine_save_information(&open_jtalk->engine, logfp);
      }
      HTS_Engine_refresh(&open_jtalk->engine);
   }
   //These are not initialized, so refreshing it may cause problems.
   //JPCommon_refresh(&open_jtalk->jpcommon);
   NJD_refresh(&open_jtalk->njd);
   Mecab_refresh(&open_jtalk->mecab);

   return result;
}

//checking whether string A begin with string B
int starts_with(char *a, const char *b)
{
   int i = 0;
   
   if(a == NULL) return FALSE;
   if(b == NULL) return FALSE;
   
   while(b[i] != 0){
      if(a[i] != b[i]) return FALSE;
      i++;
   }
   
   return TRUE;
}

//a function to do synthesis by reading a JPCommon XML file
//Parameters:
//   char *htsvoice: the name of the HTS Voice file
//   ...(options, see Open JTalk's documentation)...
//   char *infile  : the name of the XML input file
int jpcommon_xml_synthesis(
   char *htsvoice,
   char *outwav, char *outtrc,
   char *sfreq, char *fperiod, char *allpass, char *pfilter,
   char *rate, char *fm, char *unvoiced,
   char *gvspectr, char *gvlf0,
#ifdef HTS_MELP
   char *gvlpf,
#endif
   char *abuffer, char *infile
)
{
   Open_JTalk open_jtalk;
   FILE *txtfp = stdin;
   FILE *wavfp = NULL;
   FILE *logfp = NULL;
   
   // initialize Open JTalk
   Open_JTalk_initialize(&open_jtalk);

   if (htsvoice == NULL) {
      fprintf(stderr, "Error: HTS voice must be specified.\n");
      return FALSE;
   }
   
   //load HTS Engine
   if (HTS_Engine_load(&(open_jtalk.engine), &htsvoice, 1) != TRUE) {
      Open_JTalk_clear(&open_jtalk);
      return 0;
   }
   
   //open files
   if(infile != NULL) txtfp = fopen(infile, "rt");
   if(outwav != NULL) wavfp = fopen(outwav, "wb");
   if(outtrc != NULL) logfp = fopen(outtrc, "wt");
   
   //apply options
   if(sfreq    != NULL) Open_JTalk_set_sampling_frequency(&open_jtalk, (size_t) atoi(sfreq));
   if(fperiod  != NULL) Open_JTalk_set_fperiod           (&open_jtalk, (size_t) atoi(fperiod));
   if(allpass  != NULL) Open_JTalk_set_alpha             (&open_jtalk, atof(allpass));
   if(pfilter  != NULL) Open_JTalk_set_beta              (&open_jtalk, atof(pfilter));
   if(rate     != NULL) Open_JTalk_set_speed             (&open_jtalk, atof(rate));
   if(fm       != NULL) Open_JTalk_add_half_tone         (&open_jtalk, atof(fm));
   if(unvoiced != NULL) Open_JTalk_set_msd_threshold     (&open_jtalk, 1, atof(unvoiced));
   if(gvspectr != NULL) Open_JTalk_set_gv_weight         (&open_jtalk, 0, atof(gvspectr));
   if(gvlf0    != NULL) Open_JTalk_set_gv_weight         (&open_jtalk, 1, atof(gvlf0));
#ifdef HTS_MELP
   if(gvlpf    != NULL) Open_JTalk_set_gv_weight         (&open_jtalk, 2, atof(gvlpf));
#endif
   if(abuffer  != NULL) Open_JTalk_set_audio_buff_size   (&open_jtalk, (size_t) atoi(abuffer));
   
   SynthesizeFromTree(&open_jtalk, txtfp, wavfp, logfp);

   /* close files */
   if (infile != NULL)
      fclose(txtfp);
   if (wavfp != NULL)
      fclose(wavfp);
   if (logfp != NULL)
      fclose(logfp);
   
   return TRUE;
}

//a function to do synthesis like normal Open JTalk
//each option is passed as a string
int normal_mode_synthesis(
   char *dictdir, char *htsvoice,
   char *outwav, char *outtrc,
   char *sfreq, char *fperiod, char *allpass, char *pfilter,
   char *rate, char *fm, char *unvoiced,
   char *gvspectr, char *gvlf0,
#ifdef HTS_MELP
   char *gvlpf,
#endif
   char *abuffer, char *infile
)
{
   Open_JTalk open_jtalk;
   FILE *txtfp = stdin;
   FILE *wavfp = NULL;
   FILE *logfp = NULL;
   char buff[MAXBUFLEN];
   
   // initialize Open JTalk
   Open_JTalk_initialize(&open_jtalk);

   // get dictionary directory
   if (dictdir == NULL) {
      fprintf(stderr, "Error: Dictionary must be specified.\n");
      return FALSE;
   }

   // get HTS voice file name
   if (htsvoice == NULL) {
      fprintf(stderr, "Error: HTS voice must be specified.\n");
      return FALSE;
   }

   // load dictionary and HTS voice
   if (Open_JTalk_load(&open_jtalk, dictdir, htsvoice) != TRUE) {
      fprintf(stderr, "Error: Dictionary or HTS voice cannot be loaded.\n");
      Open_JTalk_clear(&open_jtalk);
      return FALSE;
   }
   
   //open files
   if(infile != NULL) txtfp = fopen(infile, "rt");
   if(outwav != NULL) wavfp = fopen(outwav, "wb");
   if(outtrc != NULL) logfp = fopen(outtrc, "wt");
   
   //apply options
   if(sfreq    != NULL) Open_JTalk_set_sampling_frequency(&open_jtalk, (size_t) atoi(sfreq));
   if(fperiod  != NULL) Open_JTalk_set_fperiod           (&open_jtalk, (size_t) atoi(fperiod));
   if(allpass  != NULL) Open_JTalk_set_alpha             (&open_jtalk, atof(allpass));
   if(pfilter  != NULL) Open_JTalk_set_beta              (&open_jtalk, atof(pfilter));
   if(rate     != NULL) Open_JTalk_set_speed             (&open_jtalk, atof(rate));
   if(fm       != NULL) Open_JTalk_add_half_tone         (&open_jtalk, atof(fm));
   if(unvoiced != NULL) Open_JTalk_set_msd_threshold     (&open_jtalk, 1, atof(unvoiced));
   if(gvspectr != NULL) Open_JTalk_set_gv_weight         (&open_jtalk, 0, atof(gvspectr));
   if(gvlf0    != NULL) Open_JTalk_set_gv_weight         (&open_jtalk, 1, atof(gvlf0));
#ifdef HTS_MELP
   if(gvlpf    != NULL) Open_JTalk_set_gv_weight         (&open_jtalk, 2, atof(gvlpf));
#endif
   if(abuffer  != NULL) Open_JTalk_set_audio_buff_size   (&open_jtalk, (size_t) atoi(abuffer));
   
   //synthesis
   fgets(buff, MAXBUFLEN - 1, txtfp);
   if (Open_JTalk_synthesis(&open_jtalk, buff, wavfp, logfp) != TRUE) {
      fprintf(stderr, "Error: waveform cannot be synthesized.\n");
      Open_JTalk_clear(&open_jtalk);
      return FALSE;
   }

   /* free memory */
   Open_JTalk_clear(&open_jtalk);

   /* close files */
   if (infile != NULL)
      fclose(txtfp);
   if (wavfp != NULL)
      fclose(wavfp);
   if (logfp != NULL)
      fclose(logfp);
   
   return TRUE;
}

//---------------------
//functions to export
//---------------------

//convert JPCommon label tree to XML format and save it as a file
//Parameters:
//   char *dictdir: the name of the directory containing dictionary
//   char *xmlout:  the name of the xml output file
//                  If it is NULL, standard output is used instead.
//   char *infile:  the name of the text input file.
//                  If it is NULL, standard input is used instead.
int write_jpcommon_xml(
   char *dictdir, char *xmlout,
   char *infile
)
{
   // text
   char buff[MAXBUFLEN];
   
   // input text file pointer
   FILE *txtfp = stdin;
   
   // output XML file pointer
   FILE *xmlfp = stdout;
   
   if (dictdir == NULL) {
      fprintf(stderr, "Error: Dictionary must be specified.\n");
      return FALSE;
   }
   if (xmlout != NULL) {
      xmlfp = fopen(xmlout, "wt");
   }
   if (infile != NULL) {
      txtfp = fopen(infile, "rt");
   }
   
   /* creating tree */
   fgets(buff, MAXBUFLEN - 1, txtfp);
   CreateJPCommonTree(dictdir, buff, "SHIFT_JIS", xmlfp);
   
   /* close files */
   if (xmlout != NULL)
      fclose(xmlfp);
   if (infile != NULL)
      fclose(txtfp);
   
   return TRUE;
}

//a function to do synthesis by reading a JPCommon XML file
//note: the text must not be passed through textString
//      as the space won't suffice
int jpcommon_xml_synthesis_jtp(jtPramater *jtp)
{
   Open_JTalk open_jtalk;
   FILE *txtfp = stdin;
   FILE *wavfp = NULL;
   FILE *logfp = NULL;
   char **strings;
   
   // initialize Open JTalk
   Open_JTalk_initialize(&open_jtalk);
   
   // get HTS voice file name
   if (jtp->hts[0] == '\0') {
      fprintf(stderr, "Error: HTS voice must be specified.\n");
      return FALSE;
   }
   
   strings = (char**)malloc(1 * sizeof(char*));
   strings[0] = (char*)malloc(256 * sizeof(char));
   strcpy(strings[0], jtp->hts);
   
   //load HTS Engine
   if (HTS_Engine_load(&(open_jtalk.engine), strings, 1) != TRUE) {
      fprintf(stderr, "Error: HTS voice cannot be loaded.\n");
      Open_JTalk_clear(&open_jtalk);
      return FALSE;
   }
   
   //open files
   if(jtp->txtfn[0] != '\0'){
      txtfp = fopen(jtp->txtfn, "rt");
   }else{
      fprintf(stderr, "Error: XML file must be specified.\n");
      return FALSE;
   }
   if(jtp->ow[0] != '\0') wavfp = fopen(jtp->ow, "wb");
   if(jtp->ot[0] != '\0') logfp = fopen(jtp->ot, "wt");
   
   //apply options
   Open_JTalk_set_sampling_frequency(&open_jtalk, jtp->s);
   Open_JTalk_set_fperiod           (&open_jtalk, jtp->p);
   Open_JTalk_set_alpha             (&open_jtalk, jtp->a);
   Open_JTalk_set_beta              (&open_jtalk, jtp->b);
   Open_JTalk_set_speed             (&open_jtalk, jtp->r);
   Open_JTalk_add_half_tone         (&open_jtalk, jtp->fm);
   Open_JTalk_set_msd_threshold     (&open_jtalk, 1, jtp->u);
   Open_JTalk_set_gv_weight         (&open_jtalk, 0, jtp->jm);
   Open_JTalk_set_gv_weight         (&open_jtalk, 1, jtp->jf);
   Open_JTalk_set_audio_buff_size   (&open_jtalk, jtp->z);
   
   SynthesizeFromTree(&open_jtalk, txtfp, wavfp, logfp);

   /* close files */
   fclose(txtfp);
   if (wavfp != NULL)
      fclose(wavfp);
   if (logfp != NULL)
      fclose(logfp);
   
   return TRUE;
}

//a function to do synthesis like normal Open JTalk
//option is passed as JPT
int normal_mode_synthesis_jtp(jtPramater *jtp)
{
   Open_JTalk open_jtalk;
   FILE *txtfp = stdin;
   FILE *wavfp = NULL;
   FILE *logfp = NULL;
   
   // initialize Open JTalk
   Open_JTalk_initialize(&open_jtalk);

   // get dictionary directory
   if (jtp->dic[0] == '\0') {
      fprintf(stderr, "Error: Dictionary must be specified.\n");
      return FALSE;
   }

   // get HTS voice file name
   if (jtp->hts[0] == '\0') {
      fprintf(stderr, "Error: HTS voice must be specified.\n");
      return FALSE;
   }

   // load dictionary and HTS voice
   if (Open_JTalk_load(&open_jtalk, jtp->dic, jtp->hts) != TRUE) {
      fprintf(stderr, "Error: Dictionary or HTS voice cannot be loaded.\n");
      Open_JTalk_clear(&open_jtalk);
      return FALSE;
   }
   
   //open files
   if(jtp->txtfn[0] != '\0'){
      txtfp = fopen(jtp->txtfn, "rt");
      fgets(jtp->textString, MAXBUFLEN - 1, txtfp);
      fclose(txtfp);
   }
   if(jtp->ow[0] != '\0') wavfp = fopen(jtp->ow, "wb");
   if(jtp->ot[0] != '\0') logfp = fopen(jtp->ot, "wt");
   
   //apply options
   Open_JTalk_set_sampling_frequency(&open_jtalk, jtp->s);
   Open_JTalk_set_fperiod           (&open_jtalk, jtp->p);
   Open_JTalk_set_alpha             (&open_jtalk, jtp->a);
   Open_JTalk_set_beta              (&open_jtalk, jtp->b);
   Open_JTalk_set_speed             (&open_jtalk, jtp->r);
   Open_JTalk_add_half_tone         (&open_jtalk, jtp->fm);
   Open_JTalk_set_msd_threshold     (&open_jtalk, 1, jtp->u);
   Open_JTalk_set_gv_weight         (&open_jtalk, 0, jtp->jm);
   Open_JTalk_set_gv_weight         (&open_jtalk, 1, jtp->jf);
   Open_JTalk_set_audio_buff_size   (&open_jtalk, jtp->z);
   
   //synthesis
   if (Open_JTalk_synthesis(&open_jtalk, jtp->textString, wavfp, logfp) != TRUE) {
      fprintf(stderr, "Error: waveform cannot be synthesized.\n");
      Open_JTalk_clear(&open_jtalk);
      return FALSE;
   }

   /* free memory */
   Open_JTalk_clear(&open_jtalk);

   /* close files */
   if (wavfp != NULL)
      fclose(wavfp);
   if (logfp != NULL)
      fclose(logfp);
   
   return TRUE;
}

//initiating parameters
int InitParam(jtPramater *jtp, char *hts)
{
   FILE *fp;
   char line[64];
   
   jtp->dic[0] = jtp->hts[0] = jtp->ot[0] = jtp->ow[0] =
      jtp->txtfn[0] = jtp->textString[0] = '\0';
   
   jtp->b = 0;
   jtp->r = 1;
   jtp->fm = 0;
   jtp->u = 0.5;
   jtp->jm = 1.0;
   jtp->jf = 1.0;
   jtp->z = 0;
   
   jtp->s = 48000;
   jtp->p = 240;
   jtp->a = 0.5;
   
   if(hts == NULL) return TRUE;
   if(hts[0] == '\0') return TRUE;
   
   strcpy(jtp->hts, hts);
   
   fp = fopen(jtp->hts, "rt");
   if(fp == NULL) return FALSE;
   
   do{
      fgets(line, 64, fp);
      if(line == NULL){
         fclose(fp);
         return FALSE;
      }
      if(starts_with(line, "SAMPLING_FREQUENCY:")){
         sscanf(line, "SAMPLING_FREQUENCY:%d", &(jtp->s));
      }else if(starts_with(line, "FRAME_PERIOD:")){
         sscanf(line, "FRAME_PERIOD:%d", &(jtp->p));
      }else if(starts_with(line, "OPTION[MCP]:")){
         sscanf(line, "OPTION[MCP]:ALPHA= %f", &(jtp->a));
      }
   }while(!starts_with(line, "[DATA]"));
   
   fclose(fp);
   
   return TRUE;
}

//new main function with mode selection
int main(int argc, char **argv)
{
   char *str_dic;  //-x dir
   char *str_hts; //-m htsvoice
   
   char *str_ow;   //-ow s
   char *str_ot;   //-ot s
   
   char *str_s;    //-s i
   char *str_p;  //-p i
   char *str_a;  //-a f
   char *str_b;  //-b f
   char *str_r;     //-r f
   char *str_fm;       //-fm f
   char *str_u; //-u f
   
   char *str_jm; //-jm f
   char *str_jf;    //-jf f
#ifdef HTS_MELP
   char *str_jl;    //-jl f
#endif                          // HTS_MELP
   
   char *str_z;  //-z i
   char *infile;
   
   //modes
   char mode = 'n'; //n: normal, w: write to xml, r: read from xml
      //normal mode: +n, or simply not stated
      //write mode: +w xmlout
      //read mode: +r
      //   the infile is XML file
   char *xmlout;
   
   //parameter
   jtPramater content;
   jtPramater *jtp = &content;
   
   str_dic = str_hts =
   str_ow = str_ot =
   str_s = str_p = str_a = str_b = str_r = str_fm = str_u =
   str_jm = str_jf =
#ifdef HTS_MELP
   str_jl =
#endif
   str_z = infile =
   xmlout = NULL;
   
   jtp = &content;
   
   if (argc == 1) usage();
   
   // get options
   while (--argc) {
      argv++;
      if (**argv == '-') {
         switch (*(*argv + 1)) {
         case 'o':
            switch (*(*argv + 2)) {
            case 'w':
               //wavfp = fopen(*++argv, "wb");
               str_ow = argv[1]; argv++;
               break;
            case 't':
               //logfp = fopen(*++argv, "wt");
               str_ot = argv[1]; argv++;
               break;
            default:
               fprintf(stderr, "Error: Invalid option '-o%c'.\n", *(*argv + 2));
               exit(1);
            }
            --argc;
            break;
         case 'h':
            usage();
            break;
         case 'x':
            str_dic = argv[1]; argv++;
            --argc;
            break;
         case 'm':
            str_hts = argv[1]; argv++;
            --argc;
            break;
         case 's':
            //Open_JTalk_set_sampling_frequency(&open_jtalk, (size_t) atoi(*++argv));
            str_s = argv[1]; argv++;
            --argc;
            break;
         case 'p':
            //Open_JTalk_set_fperiod(&open_jtalk, (size_t) atoi(*++argv));
            str_p = argv[1]; argv++;
            --argc;
            break;
         case 'a':
            //Open_JTalk_set_alpha(&open_jtalk, atof(*++argv));
            str_a = argv[1]; argv++;
            --argc;
            break;
         case 'b':
            //Open_JTalk_set_beta(&open_jtalk, atof(*++argv));
            str_b = argv[1]; argv++;
            --argc;
            break;
         case 'r':
            //Open_JTalk_set_speed(&open_jtalk, atof(*++argv));
            str_r = argv[1]; argv++;
            --argc;
            break;
         case 'f':
            switch (*(*argv + 2)) {
            case 'm':
               //Open_JTalk_add_half_tone(&open_jtalk, atof(*++argv));
               str_fm = argv[1]; argv++;
               break;
            default:
               fprintf(stderr, "Error: Invalid option '-f%c'.\n", *(*argv + 2));
               exit(1);
            }
            --argc;
            break;
         case 'u':
            //Open_JTalk_set_msd_threshold(&open_jtalk, 1, atof(*++argv));
            str_u = argv[1]; argv++;
            --argc;
            break;
         case 'j':
            switch (*(*argv + 2)) {
            case 'm':
               //Open_JTalk_set_gv_weight(&open_jtalk, 0, atof(*++argv));
               str_jm = argv[1]; argv++;
               break;
            case 'f':
            case 'p':
               //Open_JTalk_set_gv_weight(&open_jtalk, 1, atof(*++argv));
               str_jf = argv[1]; argv++;
               break;
#ifdef HTS_MELP
            case 'l':
               //Open_JTalk_set_gv_weight(&open_jtalk, 2, atof(*++argv));
               str_jl = argv[1]; argv++;
               break;
#endif                          /* HTS_MELP */
            default:
               fprintf(stderr, "Error: Invalid option '-j%c'.\n", *(*argv + 2));
               exit(1);
            }
            --argc;
            break;
         case 'z':
            //Open_JTalk_set_audio_buff_size(&open_jtalk, (size_t) atoi(*++argv));
            str_z = argv[1]; argv++;
            --argc;
            break;
         default:
            fprintf(stderr, "Error: Invalid option '-%c'.\n", *(*argv + 1));
            exit(1);
         }
      } else if (**argv == '+') { //mode selection
         switch (*(*argv + 1)) {
         case 'n': //normal mode
             break;
         case 'w': //write XML mode
             mode = 'w';
             xmlout = argv[1]; argv++; argc--;
             break;
         case 'r': //read XML mode
             mode = 'r';
             break;
         default:
            fprintf(stderr, "Error: Invalid mode '+%c'.\n", *(*argv + 1));
            exit(1);
         }
      } else {
         //txtfn = *argv;
         //txtfp = fopen(txtfn, "rt");
         infile = *argv;
      }
   }
   
   if(mode == 'n' || mode == 'r'){
      if(InitParam(jtp, str_hts) == FALSE){
         fprintf(stderr, "Cannot initiate the parameters");
         exit(1);
      }
      
      //copying string
      if(str_dic != NULL) strcpy(jtp->dic,   str_dic);
      if(str_ot  != NULL) strcpy(jtp->ot,    str_ot);
      if(str_ow  != NULL) strcpy(jtp->ow,    str_ow);
      
      //setting the input
      if(infile  != NULL){
         strcpy(jtp->txtfn, infile);
      }else{
         fscanf(stdin, "%s", jtp->textString);
      }
      
      //parsing the numbers
      if(str_s  != NULL) jtp->s  = atoi(str_s);
      if(str_p  != NULL) jtp->p  = atoi(str_p);
      if(str_a  != NULL) jtp->a  = atof(str_a);
      if(str_b  != NULL) jtp->b  = atof(str_b);
      if(str_r  != NULL) jtp->r  = atof(str_r);
      if(str_fm != NULL) jtp->fm = atof(str_fm);
      if(str_u  != NULL) jtp->u  = atof(str_u);
      if(str_jm != NULL) jtp->jm = atof(str_jm);
      if(str_jf != NULL) jtp->jf = atof(str_jf);
      if(str_z  != NULL) jtp->z  = atoi(str_z);
   }
   
   switch(mode){
   case 'n':
      if(0)
      normal_mode_synthesis(
         str_dic, str_hts,
         str_ow, str_ot,
         str_s, str_p, str_a, str_b,
         str_r, str_fm, str_u,
         str_jm, str_jf,
#ifdef HTS_MELP
         str_jl,
#endif
         str_z, infile);
      else
         normal_mode_synthesis_jtp(jtp);
      break;
   case 'w':
      write_jpcommon_xml(
         str_dic,
         xmlout, infile);
      break;
   case 'r':
      if(0)
      jpcommon_xml_synthesis(
         str_hts,
         str_ow, str_ot,
         str_s, str_p, str_a, str_b,
         str_r, str_fm, str_u,
         str_jm, str_jf,
#ifdef HTS_MELP
         str_jl,
#endif
         str_z, infile);
      else
         jpcommon_xml_synthesis_jtp(jtp);
      break;
   }
   return 0;
}
//-----------------
//Modification ends
//-----------------

OPEN_JTALK_C_END;
#endif                          /* !OPEN_JTALK_C */

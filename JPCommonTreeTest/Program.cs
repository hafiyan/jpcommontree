﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.IO;
using System.Diagnostics;
using System.Threading;

namespace OpenJTalkExtension
{
    class Program
    {
        static void Main(string[] args)
        {
            //change the paths with the appropriate ones in your environment
            string open_jtalk = @"C:\cygwin\bin\open_jtalk.exe";
            string dic = "/home/Hafiyan/open_jtalk/open_jtalk_dic_shift_jis-1.07";
            string hts_voice = "/home/Hafiyan/open_jtalk/mei_normal.htsvoice";

            //text to be synthesized
            string text = "奇跡も魔法もあるんだよ";

            //IO-related variables
            string encoding = "SHIFT_JIS";
            string infile = "input.txt";
            string xml1 = "1.xml";
            string xml2 = "2.xml";
            string wav = "out.wav";
            string trace = "trace.txt";
            TextWriter w;

            //process-related variables
            ProcessStartInfo psi;
            Process p;

            //step 1: creating input file
            w = new StreamWriter(new FileStream(infile, FileMode.Create), Encoding.GetEncoding(encoding));
            w.Write(text);
            w.Close();

            //step 2: creating readable XML file using modified Open JTalk
            psi = new ProcessStartInfo(open_jtalk);
            psi.CreateNoWindow = true;
            psi.Arguments = "+w " + xml1 + " -x " + dic + " " + infile;

            p = Process.Start(psi);
            p.WaitForExit();

            //step 3: reading the XML created by modified Open JTalk
            Tree tree = new Tree(xml1);

            //step 4: do accent / pause modifications here
            tree.MergeAllWords();
            tree.SplitAccentPhraseAndNext(0); 
            tree.SplitAccentPhraseAndNext(1);

            //step 5: save the modified tree as a new XML
            string new_xml = tree.CreateXMLText(encoding);
            w = new StreamWriter(new FileStream(xml2, FileMode.Create), Encoding.GetEncoding(encoding));
            w.Write(new_xml);
            w.Close();

            //step 6: synthesize using the new XML
            psi = new ProcessStartInfo(open_jtalk);
            psi.CreateNoWindow = true;
            psi.Arguments = "+r -m " + hts_voice + " -ow " + wav + " -ot " + trace + " " + xml2;

            p = Process.Start(psi);
            p.WaitForExit();
        }
    }
}

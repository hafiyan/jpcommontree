﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Xml.Linq;

namespace OpenJTalkExtension
{
    /// <summary>
    /// A rewritten version of JPCommonLabelPhoneme used in Open JTalk.
    /// </summary>
    public class Phoneme
    {
        const string PHONEME_NAME = "phonemename";

        //Attributes
        string _phoneme;

        //C fields
        internal Phoneme _prev, _next;
        internal Mora _up;

        //added field
        internal int _id;
        int _globalid;

        private Phoneme()
        {
            const string PAUSE_PHONEME = "pau";
            _phoneme = PAUSE_PHONEME;
        }

        /// <summary>
        /// Creates a new phoneme by reading an element from an XML document
        /// </summary>
        /// <param name="pelement">The phoneme element</param>
        public Phoneme(XElement pelement)
        {
            _phoneme = pelement.Attribute(XName.Get(PHONEME_NAME)).Value;

            _prev = _next = null;
            _globalid = -1;

        }

        /// <summary>
        /// The parent of this node.
        /// </summary>
        public Mora Up
        {
            get { return _up; }
        }

        /// <summary>
        /// The previous node.
        /// </summary>
        public Phoneme Prev
        {
            get { return _prev; }
        }

        /// <summary>
        /// The next node.
        /// </summary>
        public Phoneme Next
        {
            get { return _next; }
        }

        /// <summary>
        /// The phoneme of this node. Name changed from the original.
        /// </summary>
        public string PhonemeName
        {
            get { return _phoneme; }
        }

        /// <summary>
        /// The tree-scale global ID of this node. An added member.
        /// </summary>
        public int GlobalID
        {
            get { return _globalid; }
        }

        /// <summary>
        /// String representation of this node.
        /// </summary>
        /// <returns>The phoneme of this node.</returns>
        public override string ToString()
        {
            return _phoneme;
        }

        /// <summary>
        /// Refresh the Prev, Next, and GlobalID properties of this node.
        /// </summary>
        public void Refresh()
        {
            if (_up != null)
            {
                if (_id == 0)
                {
                    if (_up.Prev == null) _prev = null;
                    else
                    {
                        if (Up._id == 0 &&
                            Up.Up._id == 0 &&
                            Up.Up.Up._id == 0 &&
                            Up.Up.Up.Up._id > 0)
                        {
                            //the previous must be pause; do nothing
                        }
                        else
                        {
                            _prev = _up.Prev.Tail;
                        }
                    }
                }
                else
                    _prev = _up.Childs[_id - 1];
                if (_id == _up.Childs.Length - 1)
                {
                    if (_up.Next == null) _next = null;
                    else
                    {
                        if (Up._id == Up.Up.Childs.Length - 1 &&
                            Up.Up._id == Up.Up.Up.Childs.Length - 1 &&
                            Up.Up.Up._id == Up.Up.Up.Up.Childs.Length - 1 &&
                            Up.Up.Up.Up._id < Up.Up.Up.Up.Up.Childs.Length - 1)
                        {
                            Phoneme pause = new Phoneme();
                            pause._prev = this; _next = pause;
                            pause._next = _up.Next.Head; _up.Next.Head._prev = pause;
                        }
                        else
                        {
                            _next = _up.Next.Head;
                        }
                    }
                }
                else
                    _next = _up.Childs[_id + 1];
            }
            if (_prev == null) _globalid = 0;
            else _globalid = _prev._globalid + 1;

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Xml.Linq;

namespace OpenJTalkExtension
{
    /// <summary>
    /// A rewritten version of JPCommonLabelMora used in Open JTalk.
    /// </summary>
    public class Mora
    {
        const string MORA_NAME = "moraname";

        //Attributes
        string _mora;

        //C fields
        //Phoneme _head, _tail;
        internal Mora _prev, _next;
        internal Word _up;

        //added fields
        Phoneme[] _childs;
        internal int _id;
        int _globalid;

        /// <summary>
        /// Creates a new mora by reading an element from an XML document
        /// </summary>
        /// <param name="melement">The mora element</param>
        public Mora(XElement melement)
        {
            _mora = melement.Attribute(XName.Get(MORA_NAME)).Value;

            _prev = _next = null;
            _globalid = -1;

            _childs = new Phoneme[melement.Elements().Count()];
            for (int i = 0; i < _childs.Length; i++)
            {
                _childs[i] = new Phoneme(melement.Elements().ElementAt(i));
                _childs[i]._up = this;
                _childs[i]._id = i;
            }
        }

        /// <summary>
        /// The parent of this node.
        /// </summary>
        public Word Up
        {
            get { return _up; }
        }

        /// <summary>
        /// The previous node.
        /// </summary>
        public Mora Prev
        {
            get { return _prev; }
        }

        /// <summary>
        /// The next node.
        /// </summary>
        public Mora Next
        {
            get { return _next; }
        }

        /// <summary>
        /// The first child node.
        /// </summary>
        public Phoneme Head
        {
            get { return _childs[0]; }
        }

        /// <summary>
        /// The last child node.
        /// </summary>
        public Phoneme Tail
        {
            get { return _childs.Last(); }
        }

        /// <summary>
        /// The array of child nodes. An added member.
        /// </summary>
        public Phoneme[] Childs
        {
            get { return _childs; }
        }

        /// <summary>
        /// The mora of this node. Name changed from the original.
        /// </summary>
        public string MoraName
        {
            get { return _mora; }
        }

        /// <summary>
        /// The tree-scale global ID of this node. An added member.
        /// </summary>
        public int GlobalID
        {
            get { return _globalid; }
        }

        /// <summary>
        /// String representation of this node.
        /// </summary>
        /// <returns>The mora of this node.</returns>
        public override string ToString()
        {
            return _mora;
        }

        /// <summary>
        /// Refresh the Prev, Next, and GlobalID properties of this node.
        /// </summary>
        public void Refresh()
        {
            if (_id == 0)
            {
                if (_up.Prev == null) _prev = null;
                else _prev = _up.Prev.Tail;
            }
            else
                _prev = _up.Childs[_id - 1];
            if (_id == _up.Childs.Length - 1)
            {
                if (_up.Next == null) _next = null;
                else _next = _up.Next.Head;
            }
            else
                _next = _up.Childs[_id + 1];
            Mora pointer = this; int i = -1;
            do
            {
                i++; pointer = pointer._prev;
            } while (pointer != null);
            if (_prev == null) _globalid = 0;
            else _globalid = _prev._globalid + 1;
        }
    }
}

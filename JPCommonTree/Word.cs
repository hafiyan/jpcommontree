﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Xml.Linq;

namespace OpenJTalkExtension
{
    /// <summary>
    /// A rewritten version of JPCommonLabelWord used in Open JTalk.
    /// </summary>
    public class Word
    {
        const string PRON = "pron";
        const string POS = "pos";
        const string CTYPE = "ctype";
        const string CFORM = "cform";

        //Attributes
        string _pron, _pos, _ctype, _cform;

        //C fields
        //Mora _head, _tail;
        internal Word _prev, _next;
        internal AccentPhrase _up;

        //added fields
        internal Mora[] _childs;
        internal int _id;
        int _globalid;

        internal Word() { _pron = _pos = _ctype = _cform = "xx"; }
        //must initiate it, so in the XML, pron='xx'
        //scanf is not made to read empty string, so if it is not initiated,
        //there will be problem

        /// <summary>
        /// Creates a new word by reading an element from an XML document
        /// </summary>
        /// <param name="welement">The word element</param>
        public Word(XElement welement)
        {
            _pron = welement.Attribute(XName.Get(PRON)).Value;
            _pos = welement.Attribute(XName.Get(POS)).Value;
            _ctype = welement.Attribute(XName.Get(CTYPE)).Value;
            _cform = welement.Attribute(XName.Get(CFORM)).Value;

            _prev = _next = null;
            _globalid = -1;

            _childs = new Mora[welement.Elements().Count()];
            for (int i = 0; i < _childs.Length; i++)
            {
                _childs[i] = new Mora(welement.Elements().ElementAt(i));
                _childs[i]._up = this;
                _childs[i]._id = i;
            }
        }

        /// <summary>
        /// The parent of this node.
        /// </summary>
        public AccentPhrase Up
        {
            get { return _up; }
        }

        /// <summary>
        /// The previous node.
        /// </summary>
        public Word Prev
        {
            get { return _prev; }
        }

        /// <summary>
        /// The next node.
        /// </summary>
        public Word Next
        {
            get { return _next; }
        }

        /// <summary>
        /// The first child node.
        /// </summary>
        public Mora Head
        {
            get { return _childs[0]; }
        }

        /// <summary>
        /// The last child node.
        /// </summary>
        public Mora Tail
        {
            get { return _childs.Last(); }
        }

        /// <summary>
        /// The array of child nodes. An added member.
        /// </summary>
        public Mora[] Childs
        {
            get { return _childs; }
        }

        /// <summary>
        /// The pronounciation of thi word.
        /// </summary>
        public string Pron
        {
            get { return _pron; }
            set { _pron = value; }
        }

        /// <summary>
        /// The Part-of-speech type of this word.
        /// </summary>
        public string Pos
        {
            get { return _pos; }
            set { _pos = value; }
        }

        /// <summary>
        /// The conjugation type of this word.
        /// </summary>
        public string CType
        {
            get { return _ctype; }
            set { _ctype = value; }
        }

        /// <summary>
        /// The conjugation form of this word.
        /// </summary>
        public string CForm
        {
            get { return _cform; }
            set { _cform = value; }
        }

        /// <summary>
        /// The tree-scale global ID of this node. An added member.
        /// </summary>
        public int GlobalID
        {
            get { return _globalid; }
        }

        /// <summary>
        /// String representation of this node.
        /// </summary>
        /// <returns>The pronounciation of this node.</returns>
        public override string ToString()
        {
            return _pron;
        }

        /// <summary>
        /// Refresh the Prev, Next, and GlobalID properties of this node.
        /// </summary>
        public void Refresh()
        {
            if (_id == 0)
            {
                if (_up.Prev == null) _prev = null;
                else _prev = _up.Prev.Tail;
            }
            else
                _prev = _up.Childs[_id - 1];
            if (_id == _up.Childs.Length - 1)
            {
                if (_up.Next == null) _next = null;
                else _next = _up.Next.Head;
            }
            else
                _next = _up.Childs[_id + 1];
            if (_prev == null) _globalid = 0;
            else _globalid = _prev._globalid + 1;
        }
    }
}

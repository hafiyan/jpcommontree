﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Xml.Linq;

namespace OpenJTalkExtension
{
    /// <summary>
    /// A rewritten version of JPCommonLabelBreathGroup used in Open JTalk.
    /// </summary>
    public class BreathGroup
    {
        //C fields
        //internal AccentPhrase _head, _tail;
        internal BreathGroup _prev, _next;

        //added fields
        internal AccentPhrase[] _childs;
        internal Tree _up;
        internal int _id;
        int _globalid;

        internal BreathGroup() { }

        /// <summary>
        /// Creates a new breath group by reading an element from an XML document
        /// </summary>
        /// <param name="belement">The breath group element</param>
        public BreathGroup(XElement belement)
        {
            _childs = new AccentPhrase[belement.Elements().Count()];

            _prev = _next = null;
            _globalid = -1;

            for (int i = 0; i < _childs.Length; i++)
            {
                _childs[i] = new AccentPhrase(belement.Elements().ElementAt(i));
                _childs[i]._up = this;
                _childs[i]._id = i;
            }
        }

        /// <summary>
        /// The parent of this node.
        /// </summary>
        public Tree Up
        {
            get { return _up; }
        }

        /// <summary>
        /// The previous node.
        /// </summary>
        public BreathGroup Prev
        {
            get { return _prev; }
        }

        /// <summary>
        /// The next node.
        /// </summary>
        public BreathGroup Next
        {
            get { return _next; }
        }

        /// <summary>
        /// The first child node.
        /// </summary>
        public AccentPhrase Head
        {
            get { return _childs[0]; }
        }

        /// <summary>
        /// The last child node.
        /// </summary>
        public AccentPhrase Tail
        {
            get { return _childs.Last(); }
        }

        /// <summary>
        /// The array of child nodes. An added member.
        /// </summary>
        public AccentPhrase[] Childs
        {
            get { return _childs; }
        }

        /// <summary>
        /// The tree-scale global ID of this node. An added member.
        /// </summary>
        public int GlobalID
        {
            get { return _globalid; }
        }

        /// <summary>
        /// String representation of this node.
        /// </summary>
        /// <returns>The pronounciation of this node. Phrases are separated with bars.</returns>
        public override string ToString()
        {
            string temp = "";

            for (int i = 0; i < _childs.Length; i++)
            {
                temp += _childs[i];
                if (i < _childs.Length - 1) temp += "|";
            }

            return temp;
        }

        /// <summary>
        /// Refresh the Prev, Next, and GlobalID properties of this node.
        /// </summary>
        public void Refresh()
        {
            _prev = _id > 0 ? _up.Childs[_id - 1] : null;
            _next = _id < _up.Childs.Length - 1 ? _up.Childs[_id + 1] : null;
            if (_prev == null) _globalid = 0;
            else _globalid = _prev._globalid + 1;
        }
    }
}

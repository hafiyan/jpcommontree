﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.IO;
using System.Xml;
using System.Xml.Linq;
using System.Threading;

namespace OpenJTalkExtension
{
    /// <summary>
    /// A rewritten version of JPCommonLabel used in Open JTalk.
    /// </summary>
    public class Tree
    {
        //true if MergeAllWords has been executed
        bool _word_merged;

        //added fields
        BreathGroup[] _childs;

        static int _timeout = 3000;
        static int _timeloop = 500;

        /// <summary>
        /// Creates a new tree by reading an XML file.
        /// This function will try to wait if the file is still written when this function is called.
        /// The timeout limit is set in the Timeout property.
        /// </summary>
        /// <param name="xmlfile">The path of the xml to be read</param>
        public Tree(string xmlfile)
        {
            string fullpath = "";
            fullpath = Path.GetFullPath(xmlfile);

            int time_counter = 0;
            XDocument doc = null;
            while (time_counter < _timeout)
            {
                if (IsFileLocked(fullpath))
                {
                    Thread.Sleep(_timeloop);
                    time_counter += _timeloop;
                }
                else
                {
                    doc = XDocument.Load(fullpath);
                    break;
                }
            }
            if (time_counter >= _timeout)
            {
                throw new IOException();
            }

            _childs = new BreathGroup[doc.Root.Elements().Count()];

            for (int i = 0; i < _childs.Length; i++)
            {
                _childs[i] = new BreathGroup(doc.Root.Elements().ElementAt(i));
                _childs[i]._up = this;
                _childs[i]._id = i;
            }

            RestoreGlobalIDs();

            _word_merged = false;
        }

        /// <summary>
        /// The array of child nodes. An added member.
        /// </summary>
        public BreathGroup[] Childs
        {
            get { return _childs; }
        }

        /// <summary>
        /// The first breath group in this utterance.
        /// </summary>
        public BreathGroup BreathHead
        {
            get { return _childs[0]; }
        }

        /// <summary>
        /// The last breath group in this utterance.
        /// </summary>
        public BreathGroup BreathTail
        {
            get { return _childs.Last(); }
        }

        /// <summary>
        /// The first accent phrase in this utterance.
        /// </summary>
        public AccentPhrase AccentHead
        {
            get { return BreathHead.Head; }
        }

        /// <summary>
        /// The last accent phrase in this utterance.
        /// </summary>
        public AccentPhrase AccentTail
        {
            get { return BreathTail.Tail; }
        }

        /// <summary>
        /// The first word in this utterance.
        /// </summary>
        public Word WordHead
        {
            get { return AccentHead.Head; }
        }

        /// <summary>
        /// The last word in this utterance.
        /// </summary>
        public Word WordTail
        {
            get { return AccentTail.Tail; }
        }

        /// <summary>
        /// The first mora in this utterance.
        /// </summary>
        public Mora MoraHead
        {
            get { return WordHead.Head; }
        }

        /// <summary>
        /// The last mora in this utterance.
        /// </summary>
        public Mora MoraTail
        {
            get { return WordTail.Tail; }
        }

        /// <summary>
        /// The first phoneme in this utterance.
        /// </summary>
        public Phoneme PhonemeHead
        {
            get { return MoraHead.Head; }
        }

        /// <summary>
        /// The last phoneme in this utterance.
        /// </summary>
        public Phoneme PhonemeTail
        {
            get { return MoraTail.Tail; }
        }

        /// <summary>
        /// Returns the string representation of this JPCommonTree.
        /// </summary>
        /// <returns>The pronounciation of the utterance. Pauses are shown using commas.</returns>
        public override string ToString()
        {
            string temp = "";

            for (int i = 0; i < _childs.Length; i++)
            {
                temp += _childs[i];
                if (i < _childs.Length - 1) temp += ",";
            }

            return temp;
        }

        /// <summary>
        /// Creates an XML from this tree
        /// </summary>
        /// <returns>The content of the XML</returns>
        public string CreateXMLText(string encoding)
        {
            const string XML_DECLARATION_1 = "<?xml version='1.0' encoding='";
            const string XML_DECLARATION_2 = "'?>\n";
            const string UTTERANCE_NODE = "utterance";
            const string TOTAL_BREATH = "total_breath";
            const string TOTAL_ACCENT = "total_accent";
            const string TOTAL_WORD = "total_word";
            const string TOTAL_MORA = "total_mora";
            const string TOTAL_PHONEME = "total_phoneme";

            const string BREATHGROUP = "breathgroup";
            const string ACCENTPHRASE = "accentphrase";
            const string WORD = "word";
            const string MORA = "mora";
            const string PHONEME = "phoneme";

            const string ACCENT = "accent";
            const string EMOTION = "emotion";
            const string PRON = "pron";
            const string POS = "pos";
            const string CTYPE = "ctype";
            const string CFORM = "cform";
            const string MORA_NAME = "moraname";
            const string PHONEME_NAME = "phonemename";

            const string CURRENT_ID = "current_id";
            const string PREV_ID = "prev_id";
            const string NEXT_ID = "next_id";
            const string UP_ID = "up_id";
            const string HEAD_ID = "head_id";
            const string TAIL_ID = "tail_id";

            string ret = XML_DECLARATION_1 + encoding + XML_DECLARATION_2;

            ret += '<' + UTTERANCE_NODE + ' ' +
                TOTAL_BREATH + "='" + (BreathTail.GlobalID + 1) + "' " +
                TOTAL_ACCENT + "='" + (AccentTail.GlobalID + 1) + "' " +
                TOTAL_WORD + "='" + (WordTail.GlobalID + 1) + "' " +
                TOTAL_MORA + "='" + (MoraTail.GlobalID + 1) + "' " +
                TOTAL_PHONEME + "='" + (PhonemeTail.GlobalID + 1) + "' " +
                ">\n";

            for (int i = 0; i < _childs.Length; i++)
            {
                ret +=
                    "<" + BREATHGROUP + " " +
                    CURRENT_ID + "='" + i + "' " +
                    PREV_ID + "='" + (_childs[i].Prev == null ? -1 : _childs[i].Prev.GlobalID) + "' " +
                    NEXT_ID + "='" + (_childs[i].Next == null ? -1 : _childs[i].Next.GlobalID) + "' " +
                    UP_ID + "='" + 0 + "' " +
                    HEAD_ID + "='" + _childs[i].Head.GlobalID + "' " +
                    TAIL_ID + "='" + _childs[i].Tail.GlobalID + "' " +
                    ">\n";
                for (int j = 0; j < _childs[i].Childs.Length; j++)
                {
                    ret +=
                        "<" + ACCENTPHRASE + " " +
                        ACCENT + "='" + _childs[i].Childs[j].Accent + "' " +
                        EMOTION + "='" + _childs[i].Childs[j].Emotion + "' " +
                        CURRENT_ID + "='" + _childs[i].Childs[j].GlobalID + "' " +
                        PREV_ID + "='" + (_childs[i].Childs[j].Prev == null ? -1 : _childs[i].Childs[j].Prev.GlobalID) + "' " +
                        NEXT_ID + "='" + (_childs[i].Childs[j].Next == null ? -1 : _childs[i].Childs[j].Next.GlobalID) + "' " +
                        UP_ID + "='" + i + "' " +
                        HEAD_ID + "='" + _childs[i].Childs[j].Head.GlobalID + "' " +
                        TAIL_ID + "='" + _childs[i].Childs[j].Tail.GlobalID + "' " +
                        ">\n";
                    for (int k = 0; k < _childs[i].Childs[j].Childs.Length; k++)
                    {
                        ret +=
                            "<" + WORD + " " +
                            PRON + "='" + _childs[i].Childs[j].Childs[k].Pron + "' " +
                            POS + "='" + _childs[i].Childs[j].Childs[k].Pos + "' " +
                            CTYPE + "='" + _childs[i].Childs[j].Childs[k].CType + "' " +
                            CFORM + "='" + _childs[i].Childs[j].Childs[k].CForm + "' " +
                            CURRENT_ID + "='" + _childs[i].Childs[j].Childs[k].GlobalID + "' " +
                            PREV_ID + "='" + (_childs[i].Childs[j].Childs[k].Prev == null ? -1 : _childs[i].Childs[j].Childs[k].Prev.GlobalID) + "' " +
                            NEXT_ID + "='" + (_childs[i].Childs[j].Childs[k].Next == null ? -1 : _childs[i].Childs[j].Childs[k].Next.GlobalID) + "' " +
                            UP_ID + "='" + _childs[i].Childs[j].GlobalID + "' " +
                            HEAD_ID + "='" + _childs[i].Childs[j].Childs[k].Head.GlobalID + "' " +
                            TAIL_ID + "='" + _childs[i].Childs[j].Childs[k].Tail.GlobalID + "' " +
                            ">\n";
                        for (int l = 0; l < _childs[i].Childs[j].Childs[k].Childs.Length; l++)
                        {
                            ret +=
                                "<" + MORA + " " +
                                MORA_NAME + "='" + _childs[i].Childs[j].Childs[k].Childs[l].MoraName + "' " +
                                CURRENT_ID + "='" + _childs[i].Childs[j].Childs[k].Childs[l].GlobalID + "' " +
                                PREV_ID + "='" + (_childs[i].Childs[j].Childs[k].Childs[l].Prev == null ? -1 : _childs[i].Childs[j].Childs[k].Childs[l].Prev.GlobalID) + "' " +
                                NEXT_ID + "='" + (_childs[i].Childs[j].Childs[k].Childs[l].Next == null ? -1 : _childs[i].Childs[j].Childs[k].Childs[l].Next.GlobalID) + "' " +
                                UP_ID + "='" + _childs[i].Childs[j].Childs[k].GlobalID + "' " +
                                HEAD_ID + "='" + _childs[i].Childs[j].Childs[k].Childs[l].Head.GlobalID + "' " +
                                TAIL_ID + "='" + _childs[i].Childs[j].Childs[k].Childs[l].Tail.GlobalID + "' " +
                                ">\n";
                            for (int m = 0; m < _childs[i].Childs[j].Childs[k].Childs[l].Childs.Length; m++)
                            {
                                ret +=
                                    "<" + PHONEME + " " +
                                    PHONEME_NAME + "='" + _childs[i].Childs[j].Childs[k].Childs[l].Childs[m].PhonemeName + "' " +
                                    CURRENT_ID + "='" + _childs[i].Childs[j].Childs[k].Childs[l].Childs[m].GlobalID + "' " +
                                    PREV_ID + "='" + (_childs[i].Childs[j].Childs[k].Childs[l].Childs[m].Prev == null ? -1 : _childs[i].Childs[j].Childs[k].Childs[l].Childs[m].Prev.GlobalID) + "' " +
                                    NEXT_ID + "='" + (_childs[i].Childs[j].Childs[k].Childs[l].Childs[m].Next == null ? -1 : _childs[i].Childs[j].Childs[k].Childs[l].Childs[m].Next.GlobalID) + "' " +
                                    UP_ID + "='" + _childs[i].Childs[j].Childs[k].Childs[l].GlobalID + "' " +
                                    "/>\n";
                            }
                            ret += "</" + MORA + ">\n";
                        }
                        ret += "</" + WORD + ">\n";
                    }
                    ret += "</" + ACCENTPHRASE + ">\n";
                }
                ret += "</" + BREATHGROUP + ">\n";
            }
            ret += "</" + UTTERANCE_NODE + ">\n";

            return ret;
        }

        /// <summary>
        /// Severes all Prev links and Next links of the children
        /// </summary>
        public void SevereAllPrevNextLinks()
        {
            int i, j, k, l, m;

            for (i = 0; i < _childs.Length; i++)
            {
                _childs[i]._prev = _childs[i]._next = null;
                for (j = 0; j < _childs[i].Childs.Length; j++)
                {
                    _childs[i].Childs[j]._prev = _childs[i].Childs[j]._next = null;
                    for (k = 0; k < _childs[i].Childs[j].Childs.Length; k++)
                    {
                        _childs[i].Childs[j].Childs[k]._prev = _childs[i].Childs[j].Childs[k]._next = null;
                        for (l = 0; l < _childs[i].Childs[j].Childs[k].Childs.Length; l++)
                        {
                            _childs[i].Childs[j].Childs[k].Childs[l]._prev =
                                _childs[i].Childs[j].Childs[k].Childs[l]._next = null;
                            for (m = 0; m < _childs[i].Childs[j].Childs[k].Childs[l].Childs.Length; m++)
                            {
                                _childs[i].Childs[j].Childs[k].Childs[l].Childs[m]._prev =
                                    _childs[i].Childs[j].Childs[k].Childs[l].Childs[m]._next = null;
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Restores all local IDs of the children
        /// </summary>
        public void RestoreLocalIDs()
        {
            int i, j, k, l, m;

            for (i = 0; i < _childs.Length; i++)
            {
                _childs[i]._id = i;
                for (j = 0; j < _childs[i].Childs.Length; j++)
                {
                    _childs[i].Childs[j]._id = j;
                    for (k = 0; k < _childs[i].Childs[j].Childs.Length; k++)
                    {
                        _childs[i].Childs[j].Childs[k]._id = k;
                        for (l = 0; l < _childs[i].Childs[j].Childs[k].Childs.Length; l++)
                        {
                            _childs[i].Childs[j].Childs[k].Childs[l]._id = l;
                            for (m = 0; m < _childs[i].Childs[j].Childs[k].Childs[l].Childs.Length; m++)
                            {
                                _childs[i].Childs[j].Childs[k].Childs[l].Childs[m]._id = m;
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Restores the GlobalID properties of all nodes in this tree.
        /// In the meantime, also restore the Prev links and  Next links of them.
        /// </summary>
        public void RestoreGlobalIDs()
        {
            int i, j, k, l, m;
            for (i = 0; i < _childs.Length; i++)
            {
                _childs[i].Refresh();
            }
            for (i = 0; i < _childs.Length; i++)
            {
                for (j = 0; j < _childs[i].Childs.Length; j++)
                {
                    _childs[i].Childs[j].Refresh();
                }
            }
            for (i = 0; i < _childs.Length; i++)
            {
                for (j = 0; j < _childs[i].Childs.Length; j++)
                {
                    for (k = 0; k < _childs[i].Childs[j].Childs.Length; k++)
                    {
                        _childs[i].Childs[j].Childs[k].Refresh();
                    }
                }
            }
            for (i = 0; i < _childs.Length; i++)
            {
                for (j = 0; j < _childs[i].Childs.Length; j++)
                {
                    for (k = 0; k < _childs[i].Childs[j].Childs.Length; k++)
                    {
                        for (l = 0; l < _childs[i].Childs[j].Childs[k].Childs.Length; l++)
                        {
                            _childs[i].Childs[j].Childs[k].Childs[l].Refresh();
                        }
                    }
                }
            }
            for (i = 0; i < _childs.Length; i++)
            {
                for (j = 0; j < _childs[i].Childs.Length; j++)
                {
                    for (k = 0; k < _childs[i].Childs[j].Childs.Length; k++)
                    {
                        for (l = 0; l < _childs[i].Childs[j].Childs[k].Childs.Length; l++)
                        {
                            for (m = 0; m < _childs[i].Childs[j].Childs[k].Childs[l].Childs.Length; m++)
                            {
                                _childs[i].Childs[j].Childs[k].Childs[l].Childs[m].Refresh();
                                if (_childs[i].Childs[j].Childs[k].Childs[l].Childs[m]._next != null)
                                {
                                    if (_childs[i].Childs[j].Childs[k].Childs[l].Childs[m]._next.PhonemeName ==
                                        "pau")
                                    {
                                        _childs[i].Childs[j].Childs[k].Childs[l].Childs[m]._next.Refresh();
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Refreshes the properties of all nodes in this tree.
        /// </summary>
        public void Refresh()
        {
            SevereAllPrevNextLinks();
            RestoreLocalIDs();
            RestoreGlobalIDs();
        }

        /// <summary>
        /// For each accent phrase, merges all of its words and remove all of their information.
        /// </summary>
        public void MergeAllWords()
        {
            int i, j, k, l;
            string pron = "";

            SevereAllPrevNextLinks();

            for (i = 0; i < _childs.Length; i++)
            {
                for (j = 0; j < _childs[i].Childs.Length; j++)
                {
                    //for each accent phrase
                    List<Mora> mora_list = new List<Mora>();
                    AccentPhrase current_phrase = _childs[i].Childs[j];
                    Word first_word = current_phrase.Childs[0];

                    pron = current_phrase.ToString();

                    for (k = 0; k < _childs[i].Childs[j].Childs.Length; k++)
                    {
                        //for each word
                        for (l = 0; l < _childs[i].Childs[j].Childs[k].Childs.Length; l++)
                        {
                            _childs[i].Childs[j].Childs[k].Childs[l]._up = first_word;
                            mora_list.Add(_childs[i].Childs[j].Childs[k].Childs[l]);
                        }
                    }

                    first_word.Pron = pron;
                    first_word.Pos = first_word.CType = first_word.CForm = "xx";
                    first_word._childs = mora_list.ToArray();
                    current_phrase._childs = new Word[] { first_word };
                }
            }

            RestoreLocalIDs();
            RestoreGlobalIDs();

            _word_merged = true;
        }

        /// <summary>
        /// Merges a certain breath group with the breath group right after it,
        /// effectively removing the pause between them.
        /// </summary>
        /// <param name="globalid">The global ID of the breath group.</param>
        public void MergeBreathGroupWithNext(int globalid)
        {
            if (globalid >= _childs.Length - 1) throw new ArgumentOutOfRangeException("globalid");

            BreathGroup current_breath = _childs[globalid];
            List<AccentPhrase> accent_list = new List<AccentPhrase>();
            accent_list.AddRange(current_breath.Childs);
            accent_list.AddRange(current_breath.Next.Childs);
            foreach (AccentPhrase accent in accent_list)
            {
                accent._up = current_breath;
            }
            current_breath._childs = accent_list.ToArray();

            List<BreathGroup> breath_list = new List<BreathGroup>();
            breath_list.AddRange(_childs);
            breath_list.RemoveAt(current_breath.Next._id);
            _childs = breath_list.ToArray();

            RestoreLocalIDs();
            RestoreGlobalIDs();
        }

        /// <summary>
        /// Splits between a certain accent phrase and the accent phrase right after it,
        /// inserting a pause between them.
        /// </summary>
        /// <param name="globalid">The global ID of the accent phrase.</param>
        public void SplitAccentPhraseAndNext(int globalid)
        {
            int i;

            if (globalid >= AccentTail.GlobalID) throw new ArgumentOutOfRangeException("globalid");

            AccentPhrase current_accent = AccentHead;
            while (current_accent.GlobalID < globalid) current_accent = current_accent.Next;
            BreathGroup parent_breath = current_accent.Up;
            BreathGroup inserted_breath = new BreathGroup(); inserted_breath._up = this;

            if (current_accent == parent_breath.Tail) return;

            List<AccentPhrase> left_accents, right_accents;
            left_accents = new List<AccentPhrase>();
            right_accents = new List<AccentPhrase>();

            for (i = 0; i < parent_breath.Childs.Length; i++)
            {
                if (i <= current_accent._id)
                {
                    left_accents.Add(parent_breath.Childs[i]);
                }
                else
                {
                    right_accents.Add(parent_breath.Childs[i]);
                }
            }

            for (i = 0; i < left_accents.Count; i++)
            {
                left_accents[i]._up = parent_breath;
            }
            for (i = 0; i < right_accents.Count; i++)
            {
                right_accents[i]._up = inserted_breath;
            }

            parent_breath._childs = left_accents.ToArray();
            inserted_breath._childs = right_accents.ToArray();

            List<BreathGroup> breath_list = new List<BreathGroup>();
            breath_list.AddRange(_childs);
            breath_list.Insert(parent_breath.GlobalID + 1, inserted_breath);
            _childs = breath_list.ToArray();

            RestoreLocalIDs();
            RestoreGlobalIDs();
        }

        /// <summary>
        /// Merges a certain accent phrase with the accent phrase right after it.
        /// If MergeAllWords has been performed, merge the words too.
        /// </summary>
        /// <param name="globalid">The global ID of the accent phrase.</param>
        public void MergeAccentPhraseWithNext(int globalid)
        {
            if (globalid >= AccentTail.GlobalID) throw new ArgumentOutOfRangeException("globalid");

            AccentPhrase current_accent = AccentHead;
            while (current_accent.GlobalID < globalid) current_accent = current_accent.Next;
            BreathGroup parent_breath = current_accent.Up;

            if (_word_merged)
            {
                Word child_word = current_accent.Childs[0];
                List<Mora> mora_list = new List<Mora>();
                mora_list.AddRange(child_word.Childs);
                mora_list.AddRange(child_word.Next.Childs);

                foreach (Mora mora in mora_list)
                {
                    mora._up = child_word;
                }
                child_word._childs = mora_list.ToArray();
            }
            else
            {
                List<Word> word_list = new List<Word>();
                word_list.AddRange(current_accent.Childs);
                word_list.AddRange(current_accent.Next.Childs);

                foreach (Word word in word_list)
                {
                    word._up = current_accent;
                }
                current_accent._childs = word_list.ToArray();
            }

            List<AccentPhrase> accent_list = new List<AccentPhrase>();
            accent_list.AddRange(parent_breath.Childs);
            accent_list.RemoveAt(current_accent.Next._id);
            parent_breath._childs = accent_list.ToArray();

            RestoreLocalIDs();
            RestoreGlobalIDs();
        }

        /// <summary>
        /// Splits between a certain mora and the mora right after it.
        /// This will create two splitted words
        /// if MergeAllWords has not been performed,
        /// and two splitted phrase as well if it has.
        /// </summary>
        /// <param name="globalid">The global ID of the mora.</param>
        public void SplitMoraAndNext(int globalid)
        {
            int i;

            if (globalid >= MoraTail.GlobalID) throw new ArgumentOutOfRangeException("globalid");

            Mora current_mora = MoraHead;
            while (current_mora.GlobalID < globalid) current_mora = current_mora.Next;
            Word parent_word = current_mora.Up;
            Word inserted_word = new Word();

            if (current_mora == parent_word.Tail) return;

            AccentPhrase grandpa_accent = null, inserted_accent = null;

            if (_word_merged)
            {
                grandpa_accent = parent_word.Up;
                inserted_accent = new AccentPhrase();
                inserted_accent._up = grandpa_accent.Up;
                inserted_word._up = inserted_accent;

                inserted_accent._childs = new Word[] { inserted_word };
            }
            else
            {
                inserted_word._up = parent_word.Up;

            }

            List<Mora> left_morae, right_morae;
            left_morae = new List<Mora>();
            right_morae = new List<Mora>();

            for (i = 0; i < parent_word.Childs.Length; i++)
            {
                if (i <= current_mora._id)
                {
                    left_morae.Add(parent_word.Childs[i]);
                }
                else
                {
                    right_morae.Add(parent_word.Childs[i]);
                }
            }

            for (i = 0; i < left_morae.Count; i++)
            {
                left_morae[i]._up = parent_word;
            }
            for (i = 0; i < right_morae.Count; i++)
            {
                right_morae[i]._up = inserted_word;
            }

            parent_word._childs = left_morae.ToArray();
            inserted_word._childs = right_morae.ToArray();

            if (_word_merged)
            {
                List<AccentPhrase> accent_list = new List<AccentPhrase>();
                accent_list.AddRange(grandpa_accent.Up.Childs);
                accent_list.Insert(grandpa_accent._id + 1, inserted_accent);
                grandpa_accent.Up._childs = accent_list.ToArray();
            }
            else
            {
                List<Word> word_list = new List<Word>();
                word_list.AddRange(parent_word.Up.Childs);
                word_list.Insert(parent_word._id + 1, inserted_word);
                parent_word.Up._childs = word_list.ToArray();
            }

            RestoreLocalIDs();
            RestoreGlobalIDs();
        }

        /// <summary>
        /// Within an accent phrase, sets the accent type to a certain mora.
        /// </summary>
        /// <param name="globalid">The global ID of the mora.</param>
        public void SetAccentTypeAtMora(int globalid)
        {
            if (globalid >= MoraTail.GlobalID) throw new ArgumentOutOfRangeException("globalid");

            Mora current_mora = MoraHead;
            while (current_mora.GlobalID < globalid) current_mora = current_mora.Next;
            AccentPhrase grandpa_accent = current_mora.Up.Up;
            int head_id = grandpa_accent.Head.Head.GlobalID;
            grandpa_accent.Accent = globalid - head_id + 1;
        }

        /// <summary>
        /// Gets or sets the timeout property which is used when trying to open an XML file.
        /// The unit is in miliseconds.
        /// </summary>
        public static int Timeout
        {
            get { return _timeout; }
            set { _timeout = value; }
        }

        /// <summary>
        /// Gets or sets the time loop property which is used when trying to open an XML file.
        /// The unit is in miliseconds.
        /// </summary>
        public static int Timeloop
        {
            get { return _timeloop; }
            set { _timeloop = value; }
        }

        private bool IsFileLocked(string filename)
        {
            FileInfo info = new FileInfo(filename);
            FileStream stream = null;

            try
            {
                stream = info.Open(FileMode.Open, FileAccess.ReadWrite, FileShare.None);
            }
            catch (IOException)
            {
                //the file is unavailable because it is:
                //still being written to
                //or being processed by another thread
                //or does not exist (has already been processed)
                return true;
            }
            finally
            {
                if (stream != null)
                    stream.Close();
            }

            //file is not locked
            return false;
        }

        private static int CountPhonemeInNode(Mora mora)
        {
            return mora.Childs.Length;
        }

        private static int CountPhonemeInNode(Word word)
        {
            int total = 0;

            foreach (Mora mora in word.Childs)
            {
                total += CountPhonemeInNode(mora);
            }

            return total;
        }

        private static int CountPhonemeInNode(AccentPhrase accent)
        {
            int total = 0;

            foreach (Word word in accent.Childs)
            {
                total += CountPhonemeInNode(word);
            }

            return total;
        }

        private static int CountPhonemeInNode(BreathGroup breath)
        {
            int total = 0;

            foreach (AccentPhrase accent in breath.Childs)
            {
                total += CountPhonemeInNode(accent);
            }

            return total;
        }

        private static int CountPhonemeInNode(Tree utterance)
        {
            int total = 0;

            foreach (BreathGroup breath in utterance.Childs)
            {
                total += CountPhonemeInNode(breath);
            }

            return total;
        }

        private static int CountMoraInNode(Word word)
        {
            return word.Childs.Length;
        }

        private static int CountMoraInNode(AccentPhrase accent)
        {
            int total = 0;

            foreach (Word word in accent.Childs)
            {
                total += CountMoraInNode(word);
            }

            return total;
        }

        private static int CountMoraInNode(BreathGroup breath)
        {
            int total = 0;

            foreach (AccentPhrase accent in breath.Childs)
            {
                total += CountMoraInNode(accent);
            }

            return total;
        }

        private static int CountMoraInNode(Tree utterance)
        {
            int total = 0;

            foreach (BreathGroup breath in utterance.Childs)
            {
                total += CountMoraInNode(breath);
            }

            return total;
        }

        private static int CountWordInNode(AccentPhrase accent)
        {
            return accent.Childs.Length;
        }

        private static int CountWordInNode(BreathGroup breath)
        {
            int total = 0;

            foreach (AccentPhrase accent in breath.Childs)
            {
                total += CountWordInNode(accent);
            }

            return total;
        }

        private static int CountWordInNode(Tree utterance)
        {
            int total = 0;

            foreach (BreathGroup breath in utterance.Childs)
            {
                total += CountWordInNode(breath);
            }

            return total;
        }

        private static int CountAccentPhraseInNode(BreathGroup breath)
        {
            return breath.Childs.Length;
        }

        private static int CountAccentPhraseInNode(Tree utterance)
        {
            int total = 0;

            foreach (BreathGroup breath in utterance.Childs)
            {
                total += CountAccentPhraseInNode(breath);
            }

            return total;
        }

        private static int CountBreathGroupInNode(Tree utterance)
        {
            return utterance.Childs.Length;
        }

    }
}

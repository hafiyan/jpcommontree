﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Xml.Linq;

namespace OpenJTalkExtension
{
    /// <summary>
    /// A rewritten version of JPCommonLabelAccentPhrase used in Open JTalk.
    /// </summary>
    public class AccentPhrase
    {
        const string ACCENT = "accent";
        const string EMOTION = "emotion";

        //Attributes
        int _accent;
        string _emotion;

        //C fields
        //internal Word _head, _tail;
        internal AccentPhrase _prev, _next;
        internal BreathGroup _up;

        //added fields
        internal Word[] _childs;
        internal int _id;
        int _globalid;

        internal AccentPhrase() { _emotion = "(null)"; }

        /// <summary>
        /// Creates a new accent phrase by reading an element from an XML document
        /// </summary>
        /// <param name="aelement">The accent phrase element</param>
        public AccentPhrase(XElement aelement)
        {
            _accent = Int32.Parse(aelement.Attribute(XName.Get(ACCENT)).Value);
            _emotion = aelement.Attribute(XName.Get(EMOTION)).Value;

            _prev = _next = null;
            _globalid = -1;

            _childs = new Word[aelement.Elements().Count()];
            for (int i = 0; i < _childs.Length; i++)
            {
                _childs[i] = new Word(aelement.Elements().ElementAt(i));
                _childs[i]._up = this;
                _childs[i]._id = i;
            }

            //fixing accent position for flat accent
            //does not seem to be needed
            //if (_accent == 0) _accent = _childs.Length;
        }

        /// <summary>
        /// The parent of this node.
        /// </summary>
        public BreathGroup Up
        {
            get { return _up; }
        }

        /// <summary>
        /// The previous node.
        /// </summary>
        public AccentPhrase Prev
        {
            get { return _prev; }
        }

        /// <summary>
        /// The next node.
        /// </summary>
        public AccentPhrase Next
        {
            get { return _next; }
        }

        /// <summary>
        /// The first child node.
        /// </summary>
        public Word Head
        {
            get { return _childs[0]; }
        }

        /// <summary>
        /// The last child node.
        /// </summary>
        public Word Tail
        {
            get { return _childs.Last(); }
        }

        /// <summary>
        /// The position of the accent.
        /// </summary>
        public int Accent
        {
            get { return _accent; }
            set { _accent = value; }
        }

        /// <summary>
        /// The emotion of the phrase.
        /// </summary>
        public string Emotion
        {
            get { return _emotion; }
            set { _emotion = value; }
        }

        /// <summary>
        /// The array of child nodes. An added member.
        /// </summary>
        public Word[] Childs
        {
            get { return _childs; }
        }

        /// <summary>
        /// The tree-scale global ID of this node. An added member.
        /// </summary>
        public int GlobalID
        {
            get { return _globalid; }
        }

        /// <summary>
        /// String representation of this node.
        /// </summary>
        /// <returns>The pronounciation of this node.</returns>
        public override string ToString()
        {
            string temp = "";

            foreach (Word w in _childs)
            {
                temp += w;
            }

            return temp;
        }

        /// <summary>
        /// Refresh the Prev, Next, and GlobalID properties of this node.
        /// </summary>
        public void Refresh()
        {
            if (_id == 0)
            {
                if (_up.Prev == null) _prev = null;
                else _prev = _up.Prev.Tail;
            }
            else
                _prev = _up.Childs[_id - 1];
            if (_id == _up.Childs.Length - 1)
            {
                if (_up.Next == null) _next = null;
                else _next = _up.Next.Head;
            }
            else
                _next = _up.Childs[_id + 1];
            if (_prev == null) _globalid = 0;
            else _globalid = _prev._globalid + 1;
        }
    }
}

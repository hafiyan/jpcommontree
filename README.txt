JPCommonTree
(version 1.0 beta)
Sendai, 26 October 2014

This project contains a C# data structure I call JPCommonTree.
This data structure is a rewrite of JPCommonLabel data structure used in Open JTalk to get rid of pointers,
and for better compatibility with other C# codes.

Open JTalk is a Japanese TTS System, designed to synthesize speech from Japanese language text.
It has been being developed by HTS working group and some graduate students in Nagoya Institute of Technology.
It is a pretty solid software, but sometimes it has some problems.
Among them is inability to give the reading of the kanji,
and inability to set the accent, which for some might feel unnatural.
This project is made to complement Open JTalk,
giving it an ability to set the accent easily.

This project is released into public domain under no license and no warranty.

Contributors:
-Hafiyan Prafianto
-Bi Yu

Files and directories in this directory:
Name               Description
JPCommonTree\      The main library
JPCommonTreeTest\  Sample code to use the library
C_files\           Source code for the modification to be made on Open JTalk and an instruction
JPCommonTree.sln   Visual C# 2012 solution file
